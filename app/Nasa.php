<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\GuzzleRequests;

class Nasa extends Model
{

    function aprod($data = array()){
        try {
            $request = new GuzzleRequests();
            $headers = ['Content-type' => 'application/json'];

            $result = $request->get(
                $headers,                               //headers da requisicao, no caso eles nao pedem nenhuma
                'https://api.nasa.gov/planetary/apod',                          //link para a requisicao no endpoint
                [                                       //abaixo sao os parametros que eles pedem para dar o retorno
                    'date' => $data['date'],            //default => today, formato padrao yyyy-mm-dd
                    'hd' => $data['hd'],                //Se quer apenas a imagem em alta definicao
                    'api_key' => $data['api_key']
                ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => $e->getCode(),
                'error' => $e->getMessage()
            ]);
        }

        return response()->json([
            'status' => 200,
            'date' => $result->date,
            'description' => $result->explanation,
            'image_hd' => $result->hdurl,
            'title' => $result->title,
            'normal_image' => $result->url
        ]);
    }
}
