<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Nasa;

class NasaController extends Controller
{
    public function aprod(Request $request, Nasa $nasa)
    {
        $request->validate([
            'date' => 'nullable|date',
            'hd' => 'nullable',
            'api_key' => 'required'
        ]);

        $nasa->aprod($request->all());
    }
}
