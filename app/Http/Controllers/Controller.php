<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $apikey = 'U2zBEnmm6ZM09w7qD7A3p0cFtzrHWdYrT9keYMOe';
    public $url_apod = 'https://api.nasa.gov/planetary/apod';
}
