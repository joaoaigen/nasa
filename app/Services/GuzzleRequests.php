<?php

namespace App\Services;

use GuzzleHttp\Client;

class GuzzleRequests
{
    public function get($headers = array(), $link, $query = array())
    {
        $client = new Client([
            'headers' => $headers
        ]);

        $res = $client->request('GET', $link,
            ['query' =>  $query]);
        $dados = json_decode($res->getBody()->getContents());

        return $dados;
    }

    public function post($headers = array(), $link, $body = array())
    {
        $client = new Client([
            'headers' => $headers
        ]);

        $res = $client->request('POST', $link,
            ['body' =>  $body]);
        $dados = json_decode($res->getBody()->getContents());

        return $dados;
    }
}
